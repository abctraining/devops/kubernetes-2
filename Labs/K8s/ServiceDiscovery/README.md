# Service Discovery: Lab

We will use the 'yaml' files from the repos `Labs/K8s/ServiceDiscovery/` directory.

Let us create a service names `thesvc` and a Replication Controller supervisng some pods.

```bash
kubectl apply -f rc.yaml
kubectl apply -f svc.yaml
```

Now we want to connect to the `thesvc` service from within the cluster, say, from another service. To simulate this, we create a jump pod in the same namespace (default, since we didn’t specify anything else).

```bash
kubectl apply -f jumpod.yaml
```
The **DNS add-on** will make sure that our service thesvc is available via the FQDN `thesvc.default.svc.cluster.local` (Change the "cluster.local" name to your cluster name if needed) from other pods in the cluster. Let’s try it out:

```bash
kubectl exec -it jumpod -c shell -- ping -c3 thesvc.default.svc.cluster.local
```

_output_
```
PING thesvc.default.svc.cluster.local (10.43.97.33) 56(84) bytes of data.

--- thesvc.default.svc.jtclark.local ping statistics ---
3 packets transmitted, 0 received, 100% packet loss, time 2046ms

command terminated with exit code 1
```

The answer above to the ping tells us that the service is resolved to the cluster IP 172.30.251.137. The ping was not allowed to a Service Cluster IP. We can directly connect to and consume the service (in the same namespace) like so:

```bash
kubectl exec -it jumpod -c shell -- curl http://thesvc/info
```

_output_
```
{"host": "thesvc", "version": "0.5.0", "from": "10.42.0.97"}
```

Note that the IP address 10.42.0.97 above is the cluster-internal IP address of the jump pod.

To access a service that is deployed in a different namespace than the one you’re accessing it from, use a FQDN in the form `$SVCNAME.$NAMESPACE.svc.$CLUSTERNAME.local.` (the default clustername=cluster, so the cluster domain is cluster.local)

Let’s see how that works by creating:

* a namespace `other`
* a service thesvc in namespace other
* an RC supervising the pods, also in namespace other

```bash
kubectl apply -f other-ns.yaml
kubectl apply -f other-rc.yaml
kubectl apply -f other-svc.yaml
```

We're now in the position to consume the service `thesvc` in namespace `other` from the `default` namespace (again via the jump pod):

```bash
kubectl exec -it jumpod -c shell -- curl http://thesvc.other/info
```

_output_
```
{"host": "thesvc.other", "version": "0.5.0", "from": "10.42.0.97"}
```

Summing up, DNS-based service discovery provides a flexible and generic way to connect to services across the cluster.

You can destroy all the resources created with:

```bash
kubectl delete -f rc.yaml
kubectl delete -f svc.yaml
kubectl delete -f other-svc.yaml
kubectl delete -f other-rc.yaml
kubectl delete -f other-ns.yaml
kubectl delete -f jumpod.yaml
```
