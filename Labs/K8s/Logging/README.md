# K8s Basic Logging

We will use the 'yaml' files from the repos `Labs/K8s/Logging/` directory for the following exercises.

Run a pod and check logs

```bash
kubectl create -f basic-logging.yaml
```

Check logs

```bash
kubectl logs pod/counter
```

Follow the logs from the pod

```bash
kubectl logs -f pod/counter
```

'CTRL+C' to exit

Destroy all resources

```bash
kubectl delete -f basic-logging.yaml
```
