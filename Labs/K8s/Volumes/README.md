# Creating Volumes

### Create new pod with shared volume

This labs files are located in the repos `Labs/K8s/Volumes/` folder

Create a pod with multiple containers with shared volume

```bash
kubectl create -f pod.yaml
kubectl describe pod sharevol
```

check the `Mounts` in each container. You will see something like `/var/run/secrets/kubernetes.io/serviceaccount`

Go to a container and check the path, you will see `ca.cert`, `namespace` and `token`. The three files in both containers will be same, as this is the token being used to authenticate the shared volume across both containers


Exec into the containers c1 and save some data

```bash
kubectl exec sharevol -it -c c1 -- bash
```
From the shell connected inside the Pod

```bash
echo 'some data' > /tmp/xchange/data
```

Now `exit` the container and exec into the second container

```bash
kubectl exec sharevol -it -c c2 -- bash
```

Once inside c2

```bash
cat /tmp/data/data
```

We are finished in the container so type `exit`.

### Cleanup
Delete all pods and volumes

```bash
kubectl delete po sharevol
```
