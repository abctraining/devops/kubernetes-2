## Namespaces Usage

In this lab we will use the 'yaml' files from the repos `Labs/K8s/Namespaces/` directory.

Create namespaces and run pods

_create two namespaces_

```bash
kubectl create -f dev-ns.yaml
kubectl create -f prod-ns.yaml
```

_create two pods with 2 namespaces_

```bash
kubectl create -f dev-pod.yaml -n dev
kubectl create -f prod-pod.yaml -n prod
```

_Search for pods_

```bash
kubectl get pods
kubectl get pods -n dev
kubectl get pods -n prod

```

---

Restrict accessing prod pod from other namespace

```bash
kubectl create -f deny-from-other-ns.yaml
```

This blocks accessing `prod` pod from any other namespace with a NetworkPolicy.

_verify_
```bash
kubectl get pods -o wide -n dev nginx-dev
kubectl get pods -o wide -n prod nginx-prod
```

copy the IP of the two pods (nginx-dev and nginx-prod)

From nginx-prod

```bash
kubectl exec -it nginx-prod -n prod -- /bin/bash
```

_on the nxinx-prod pod_
```bash
apt-get update
apt-get install -y iputils-ping
ping -c3 <nginx-dev-ip>
ping -c3 <nginx-prod-ip>
```
_both should work_
_use `exit` to leave the pod_


From nginx-dev

```bash
kubectl exec -it nginx-dev -n dev -- /bin/bash
```

_on the nginx-dev pod_
```bash
apt-get update
apt-get install -y iputils-ping
ping -c3 <nginx-dev-ip>
ping -c3 <nginx-prod-ip>
```
_ping to prod should not work_

_use `exit` to leave the pod_

## cleanup

```bash
kubectl delete ns dev
kubectl delete ns prod
```
