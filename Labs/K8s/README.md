# Kubernetes Labs

* [00 - Setup K8s Cluster](K8sSetup)
* [01 - RC, RS, & Deployment](Deployments)
* [02 - Services](Services)
* [03 - Volumes](Volumes)
* [04 - PersistentVolumes](PersistentVolumes)
* [05 - Labels](Labels)
* [06 - Namespaces](Namespaces)
* [07 - ConfigMaps](ConfigMaps)
* [08 - Jobs](Jobs)
* [09 - Autoscaling](Autoscaling)
* [10 - Service Discover](ServiceDiscovery)
* [11 - Monitoring](Monitoring)
* [12 - Logging](Logging)
* [13 - Helm](Helm)
* [14 - EFK](EFK)


---

###### Lab Files

_Copy Lab repo local_
```bash
cd ~/
git clone https://gitlab.com/abctraining/devops/kubernetes-2.git
cd kubernetes-2
```
