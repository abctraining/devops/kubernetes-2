# Logging using EFK

We will use the 'yaml' files from the repos `Labs/K8s/EFK/` directory.

---

##### Deploy ES, Kibana and fluentd bit roles

### ElasticSearch

_Set needed sysctl vaules_
```bash
sudo sysctl -w vm.max_map_count=262144 && sudo sysctl -w fs.file-max=65536
```

_Create Namespace_
```bash
kubectl create ns logging
```

##### Install ElasticSearch

_Create and Expose ElasticSearch Deployment_
```bash
kubectl create deployment elasticsearch --image=docker.elastic.co/elasticsearch/elasticsearch:6.8.18 -n logging
kubectl expose deploy elasticsearch --port 9200 -n logging
```

__or__ use Helm, __Don't do this unless you have at least 8GB of memory.__  So in the context of the class use the above commmand to install an older Elasticsearch. This takes a number of resources and may overload small clusters.

_install ElasticSearch with Helm_ __Do not do this following command unless you have lots of resources on your cluster__
```bash
helm install --namespace logging elasticsearch bitnami/elasticsearch --set volumePermissions.enabled=true --set master.replicas=1 --set coordinating.replicas=1 --set data.replicas=1 --set ingest.replicas=1

```

###### verify
```bash
kubectl get all -o wide -n logging
curl <es-svc-ClusterIP>:9200
```

---

### Kibana

###### Cleaning

Check if you have previous version of helm kibana installed
```bash
helm list --namespace logging
```
if you see kibana, delete it.  _This would exist if you have run through these steps before_

```bash
helm uninstall kibana -n logging
```
---

##### Install Kibana

```bash
helm repo add elastic https://helm.elastic.co
helm repo update
```

```bash
helm install kibana elastic/kibana --namespace logging --set elasticsearchHosts="http://elasticsearch:9200" --set imageTag="6.8.18"
```


_Verify_
```bash
curl -D - <kibana-svc-ClusterIP>:5601
```

_More verifying_
```bash
kubectl -n logging get pods -l "app=kibana"
kubectl get all -n logging
```

---

### Fluentd

##### Install Fluentd

```bash
kubectl apply -f fluent-bit-service-account.yaml
kubectl apply -f fluent-bit-role.yaml
kubectl apply -f fluent-bit-role-binding.yaml
```

###### Config Map
This config map will be used as the base configuration of the Fluent Bit container. Keywords such as INPUT, OUTPUT, FILTER, and PARSER in this file are used.

_Fluentd configmap_
```bash
kubectl apply -f fluent-bit-configmap.yaml
```

_Fluentd daemonset_
```bash
kubectl apply -f fluent-bit-ds.yaml
```

_verify: you should see elasticsearch, fluent-bit and kibana pods_
```bash
kubectl get pods -n logging
```

---

#### Populate logs


```bash
kubectl create deployment nginx --image=nginx -n logging
kubectl expose deploy nginx --port 80 -n logging
```

_curl few times_
```bash
curl <nginx-svc-ClusterIP>:80
```

_check logs in ES_
```bash
curl <es-svc-ClusterIP>:9200/_cat/indices?v
```

copy the log-stash-index name

```bash
curl http://<es-svc-ClusterIP>:9200/<logstash-index-name>/_search?pretty=true&q={'matchAll':{''}}
```

---

change the ES type of service (change ClusterIP to NodePort)

```bash
kubectl edit service/elasticsearch -n logging
```

change the kibana type of service (change ClusterIP to NodePort)

```bash
kubectl edit service/kibana-kibana -n logging
```

##### Check in browser
1. `http://<public-ip>:<kibana-nodeport>`  # (select "explore my own")
2. Go to `Management` -> `Index Patterns`
3. Search `logstash*` -> next step -> select `@timestamp` in configure settings -> `create index patterns`
4. Go to `discover` and discover logs


### Cleanup

```bash
kubectl delete ns logging
```

```bash
helm ls --all
```

if you find kibana in helm, delete and purge it

```bash
helm del --purge kibana
```

---

#### Alternative to not changing the kibana service type

This requires `kubectl` be running from the local client

```bash
export POD_NAME=$(kubectl get pods --namespace logging -l "app=kibana,release=kibana" -o jsonpath="{.items[0].metadata.name}")
echo "Visit http://127.0.0.1:5601 to use Kibana"
kubectl port-forward --namespace logging $POD_NAME 5601:5601
```
