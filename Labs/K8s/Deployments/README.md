## Replication Controller

Create a Replication Controller from a file called rc.yaml. The rc.yaml file is located in the lab repo `Labs/K8s/Deployments/` folder.  The lab repo can be cloned locally with the commands found at the end of the [K8s Lab page](/Labs/K8s).

Most of this structure should look familiar from our discussion of Deployments; we’ve got the name of the actual
Replication Controller (soaktestrc) and we’re designating that we should have 3 replicas, each of which are
defined by the template.  The selector defines how we know which pods belong to this Replication Controller.

Now tell Kubernetes to create the Replication Controller based on that file:

```bash
kubectl create -f rc.yaml
```

###### _output:_
```
replicationcontroller "soaktestrc" created
```

Let’s take a look at what we have using the describe command:

```bash
kubectl describe rc soaktestrc
```

###### _output:_
```
Name:           soaktestrc
Namespace:      default
Image(s):       nickchase/soaktest
Selector:       app=soaktestrc
Labels:         app=soaktestrc
Replicas:       3 current / 3 desired
Pods Status:    3 Running / 0 Waiting / 0 Succeeded / 0 Failed
No volumes.
Events:
  FirstSeen     LastSeen        Count   From                            SubobjectPath   Type   Reason                   Message
  ---------     --------        -----   ----                            -------------   --------------                  -------
  1m            1m              1       {replication-controller }                       Normal SuccessfulCreate Created pod: soaktestrc-g5snq
  1m            1m              1       {replication-controller }                       Normal SuccessfulCreate Created pod: soaktestrc-cws05
  1m            1m              1       {replication-controller }                       Normal SuccessfulCreate Created pod: soaktestrc-ro2bl
```

As we can see, we’ve got the Replication Controller, and there are 3 replicas (pods), of the 3 pods that we wanted.
All 3 of them are currently running.  You can also see the individual pods listed underneath, along with their names.  We added the --show-labels options and -l option for selecting by labels

```bash
kubectl get rc,po --show-labels -l app=soaktestrc
```

###### _output:_
```
NAME               READY     STATUS    RESTARTS   AGE
soaktestrc-cws05   1/1       Running   0          3m
soaktestrc-g5snq   1/1       Running   0          3m
soaktestrc-ro2bl   1/1       Running   0          3m
```

Delete one of the pods (out of 3)
If you delete one of the pods manually, then the replication controller will create one to match the desired numbers of pods to be 3.

```bash
kubectl delete pod/<pod_name>
```

###### _output:_
```
pod "<pod_name>" deleted
```

Check the pods Again
```bash
kubectl get pods -o wide
```
You will see three pods again

Clean up the Replication Controllers

```bash
kubectl delete rc soaktestrc
```

###### _output:_
```
replicationcontroller "soaktestrc" deleted
```

check if the pods got deleted

```bash
kubectl get rc,po --show-labels -l app=soaktestrc
```

###### _output:_
```
No resources found in default namespace.

```
As you can see, when you delete the Replication Controller, you also delete all of the pods that it created.

---

## Replica Sets
Next we’ll look at Replica Sets, but first let’s clean up:

Replica Sets are a sort of hybrid, in that they are in some ways more powerful than Replication Controllers,
and in others they are less powerful. Replica Sets are declared in essentially the same way as Replication Controllers,
except that they have more options for the selector. Use the file rs.yaml for this part

__The first rs.yaml file is similar to rc.yaml__

###### rs.yaml

```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: soaktestrs
spec:
  replicas: 3
  selector:
    matchLabels:
      app: soaktestrs
  template:
    metadata:
      labels:
        app: soaktestrs
        environment: dev
    spec:
      containers:
      - name: soaktestrs
        image: nickchase/soaktest
        ports:
        - containerPort: 80
```

Go ahead and create the replica-set, describe and delete it.
You may chose to skip this part, as this replica-set is similar to replication-controller we checked earlier.


### Replica set with more options

###### rs_selector2.yaml

```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: soaktestrs1
spec:
  replicas: 3
  selector:
   matchExpressions:
     - {key: app, operator: In, values: [soaktestrs, soaktestrs, soaktest]}
     - {key: tier, operator: NotIn, values: [production]}
  template:
    metadata:
      labels:
        app: soaktestrs
        environment: dev
    spec:
      containers:
      - name: soaktestrs
        image: nickchase/soaktest
        ports:
        - containerPort: 80
```

In this case, it’s more or less the same as when we were creating the Replication Controller, except we’re using matchLabels instead of label.  But we could just as easily have said (has matchExpressions):


Two different conditions defined in matchExpressions:
>  - The app label must be soaktestrs, soaktestrs or soaktest <br>
>  - The tier label (if it exists) must not be production

Let’s go ahead and create the Replica Set and get a look at it:

```bash
kubectl create -f rs_selector2.yaml
```

###### _output:_
```
replicaset.extensions/soaktestrs1 created
```
We can describe the Replica Sets

```bash
kubectl describe replicaset soaktestrs1
```

###### _output:_
```
Name:         soaktestrs1
Namespace:    default
Selector:     app in (soaktest,soaktestrs,soaktestrs),tier notin (production)
Labels:       app=soaktestrs
              environment=dev
Annotations:  <none>
Replicas:     3 current / 3 desired
Pods Status:  3 Running / 0 Waiting / 0 Succeeded / 0 Failed
Pod Template:
  Labels:  app=soaktestrs
           environment=dev
  Containers:
   soaktestrs:
    Image:        nickchase/soaktest
    Port:         80/TCP
    Host Port:    0/TCP
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Events:
  Type    Reason            Age   From                   Message
  ----    ------            ----  ----                   -------
  Normal  SuccessfulCreate  27s   replicaset-controller  Created pod: soaktestrs1-7xmr5
  Normal  SuccessfulCreate  27s   replicaset-controller  Created pod: soaktestrs1-m2fqj
  Normal  SuccessfulCreate  27s   replicaset-controller  Created pod: soaktestrs1-rt68h  
```

We can get the pods

```bash
kubectl get pods
```

###### _output:_
```
NAME               READY     STATUS    RESTARTS   AGE
soaktestrs1-7xmr5   1/1     Running   0          74s
soaktestrs1-m2fqj   1/1     Running   0          74s
soaktestrs1-rt68h   1/1     Running   0          74s
```

As we can see, the output is pretty much the same as for a Replication Controller (except for the selector),
and for most intents and purposes, they are similar.  

##### Cleanup

```bash
kubectl delete replicaset soaktestrs1
```

__Explore bad options__ :- To see the errors when the labels and selectors does not match , feel free to try out the other replica set in the same location that has a mis-matched label:-

This should give an error

```bash
kubectl create -f rs_selector2_bad.yaml
```

###### _output:_
```
The ReplicaSet "soaktestrsbad" is invalid: spec.template.metadata.labels: Invalid value: map[string]string{"app":"soaktestrspq", "tier":"prod"}: `selector` does not match template `labels`
```

###### Fix:

Add the required label to the matchExpression like below:

```yaml
matchExpressions:
     - {key: app, operator: In, values: [soaktestrs, soaktestrs, soaktest, soaktestrspq]}
```

Create the resource again

##### Cleanup

```bash
kubectl delete rs soaktestrsbad
```

###### _output:_
```
# replicaset "soaktestrs" deleted
```

```bash
kubectl delete rs soaktestrs1
```

###### _output:_
```
# replicaset "soaktestrs1" deleted
```

```bash
kubectl delete rs soaktestrsbad
```

###### _output:_
```
# replicaset "soaktestrsbad" deleted
```

Again, the pods that were created are deleted when we delete the Replica Set.

```bash
kubectl get all -o wide
```

--- With the exemption of a service named kubernetes there shouldn't be any other resource.

---

## Deployments

Deployments are intended to replace Replication Controllers.  They provide the same replication functions
(through Replica Sets) and also the ability to rollout changes and roll them back if necessary.

You first have to create a simple Deployment (you can find it in the git repo)  

`deploy_backed_by_rs.yaml`

Now go ahead and create the Deployment:

```bash
kubectl create -f deploy_backed_by_rs.yaml
```

###### _output:_
```
deployment.extensions/soaktest created
```

Now let’s go ahead and describe the Deployment:

```bash
kubectl describe deployment soaktest
```

###### _output:_
```
Name:                   soaktest
Namespace:              default
CreationTimestamp:      Sun, 05 Mar 2017 16:21:19 +0000
Labels:                 app=soaktest
Selector:               app=soaktest
Replicas:               5 updated | 5 total | 5 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  1 max unavailable, 1 max surge
OldReplicaSets:         <none>
NewReplicaSet:          soaktest-3914185155 (5/5 replicas created)
Events:
  FirstSeen     LastSeen        Count   From                            SubobjectPath   Type    Reason                   Message
  ---------     --------        -----   ----                            -------------   --------------                   -------
  38s           38s             1       {deployment-controller }                        Normal  ScalingReplicaSet        Scaled up replica set soaktest-3914185155 to 3
  36s           36s             1       {deployment-controller }                        Normal  ScalingReplicaSet        Scaled up replica set soaktest-3914185155 to 5
  ```

As you can see, rather than listing the individual pods, Kubernetes shows us the Replica Set.  
Notice that the name of the Replica Set is the Deployment name and a hash value.

We can check the pods for the deployment (and also behind the scenes the dpeloyment creates a replica sets)


Check all deployments, pods, RCs, RSs

```bash
kubectl get deploy,po,rs,rc
```

###### _output:_
```
NAME                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/soaktest   5/5     5            5           42s

NAME                            READY   STATUS    RESTARTS   AGE
pod/soaktest-5d78c4cfcc-875tn   1/1     Running   0          42s
pod/soaktest-5d78c4cfcc-lx8fw   1/1     Running   0          42s
pod/soaktest-5d78c4cfcc-n59wb   1/1     Running   0          42s
pod/soaktest-5d78c4cfcc-v7gkl   1/1     Running   0          42s
pod/soaktest-5d78c4cfcc-zxkrc   1/1     Running   0          42s

NAME                                  DESIRED   CURRENT   READY   AGE
replicaset.apps/soaktest-5d78c4cfcc   5         5         5       42s
```

You can see that there is 1 deployment, 5 pods (because of 5 replication), 1 replica set BUT no replication controller.
This is because deployment created replica set but not replication controller

##### Cleanup

```bash
kubectl delete deployment.apps/soaktest
```

_This deletes deployment, resplica-set and pods_
