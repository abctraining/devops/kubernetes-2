# Single Node Cluster using kubeadm

#### Prepare the machine

The VM should have at least 2 vCPUs

_update local package repo_
```bash
sudo apt update
```

_add additional packages_
```bash
sudo apt install -y bash-completion software-properties-common
```

_disable swap_
```bash
sudo swapoff -a
```

#### Install Docker (Unless Docker is already installed)


Choose __ONE__ of the following scripts to install docker, __NOT BOTH__

* get.docker.com
```bash
curl -fsSL https://get.docker.com | sudo VERSION=20.10 sh
```

__or__

* Rancher provided script_
```bash
curl https://releases.rancher.com/install-docker/20.10.sh | sudo sh
```

##### Docker Group

Regardless of which script you used add your user to the docker group:

_add user to the docker group_
```bash
sudo usermod -aG docker $USER
```

__Note: You must logout and log back in to get the local shell to recognize the newly added 'docker' group__

---

#### Add Kubernetes Package Repositories

_Add Kubernetes Package Repository_
```bash
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"
```


#### Install K8s packages

_Install kubectl_
```bash
sudo apt-get install -y kubelet=1.21.3-00 kubeadm=1.21.3-00 kubectl=1.21.3-00 kubernetes-cni=0.8.7-00
```

#### Initilize the ClusterIP

_cluster init_
```bash
sudo kubeadm init --kubernetes-version=1.21.3 --pod-network-cidr=10.128.0.0/16
```

__NOTE: Save the 'kubeadm join' string for later if you plan on joining additionl nodes to the cluster__

#### Configure kubectl

_add the admin config for kubectl_
```bash
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

_Verify kubectl binary_
```bash
kubectl version
kubectl get nodes
```

_Note: The status of the node will be "NotReady" as there is no network provider.

#### Add Network Provider

```bash
export KUBEVERBASE64=$(kubectl version | base64 | tr -d '\n')
kubectl apply -f https://cloud.weave.works/k8s/net?k8s-version=${KUBEVERBASE64}
```
#### Verify kubernetes master
```bash
kubectl get nodes -o wide
kubectl get pods --all-namespaces -o wide
kubectl get pods,deployments --all-namespaces -o wide
```

#### check kube config file
```bash
cat .kube/config
kubectl cluster-info
```

It can be helpful to enable tab-completion for 'kubectl'.  These commands will enable it.

```bash
source <(kubectl completion bash)
echo 'source <(kubectl completion bash)' >>~/.bashrc
```

---

So far we have installed and configured the master node. But you won't be able to schedule pods yet. Because the cluster doesn't have any worker node, and in case you need just a single node cluster, your master node is not configured to schedule pods.

Let's test it.

#### pod scheduling won't work. Let's make it work
```bash
kubectl run nginx --image=nginx --replicas=3

# get the <ip-x-x-x-x> by running the command [kubectl get nodes -o wide]
kubectl describe nodes <ip-x-x-x-x>
# check the output of describe -- you will find "Taints: node-role.kubernetes.io/master:NoSchedule"

# delete the deployment we just created
kubectl delete deployment.extensions/nginx

# use master for scheduling
ip=`kubectl get nodes | grep master | awk -F" " '{print $1}'`
kubectl taint nodes $ip node-role.kubernetes.io/master:NoSchedule-

# describe master node again and check the difference. you will find "Taints: <none>"
kubectl describe nodes <ip-x-x-x-x>

# schedule pod again (via deployment)
kubectl run nginx --image=nginx --replicas=3

# check pods and deployment, you will find it now.
kubectl get pods -o wide
kubectl get deployment -o wide
```

#### How to access nginx server? - Need to expose as service
```bash
kubectl expose deployment nginx --port=80 --type=LoadBalancer
kubectl get deployments,svc -o wide
```

#### verify nginx
```bash
curl <public-ip>:<exposed port>
OR
curl <cluster-ip>:80
```
<br>

#### clean-up
```bash
kubectl delete service nginx
kubectl delete deployment nginx
## wait for 5 seconds
kubectl get all
```


<br><br>
### Advanced 1: How to setup multi-node kube cluster - just discussion, no lab required
> Install docker, kubeadm, kubelet in worker nodes - same as master <br>
> In the worker: DO NOT "kubeadm init" and DO NOT "copy .kube/config" to home folder <br>
> Get token from master by running the command
[kubeadm  token create --print-join-command]
> Run the output of the above command in the worker nodes
> DO NOT taint master for scheduling

<br><br>
### Advanced 2: How to configure access to multiple clusters - just discussion, no lab required
#### Configure local kubectl to connect to another master kube cluster - copy "cluster CA certificate" of the master cluster to a file {remote.cluster.ca.cert}
	- kubectl config set-cluster master-cluster --server=<endpoint> --certificate-authority=remote.cluster.ca.cert
	- kubectl config set-credentials master-cluster-admin --username <username> --password <password>
	- verify by "kubectl config view"
	* kubectl will still pull local kube cluster information if you run "kubectl get" commands
	- create a context: kubectl config set-context frontend --cluster=master --namespace=frontend
	- switch to the new context: kubectl config use-context frontend
	* kubectl will now switch to new context and connect to master cluster.
	* Check kubectl config help to see different options to configure local kubectl [kubectl config --help]
