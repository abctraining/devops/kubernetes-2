# Kubernetes Setup

Kubernetes Setup on AWS


##### K8s Setup Options
_(use "K3s Single Node Cluster" unless you have reason to choose something different)_

* [kubeadm - Single Node Cluster](kubeadm-SingleNodeCluster.md)
* [RKE - Single Node Cluster](RKE-SingleNodeCluster.md)
* [K3s - Single Node Cluster](K3s-SingleNodeCluster.md)
* [MicroK8s - SingleNodeCluster](MicroK8s-SingleNodeCluster.md)

##### Optional Addons

* [Kubernetes Dashboard](k8s-Dashboard.md)
* [MetalLB](MetalLB.md)
* [Rancher](Rancher.md)
* [OpenEBS](OpenEBS.md)
