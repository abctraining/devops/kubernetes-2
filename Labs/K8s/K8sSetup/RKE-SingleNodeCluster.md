# Single Node Cluster using RKE

###### System Prep

_Generate SSH Keys (Select Defaults)_
```bash
ssh-keygen
```

_Copy Public SSH key to authorized_keys_
```bash
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
```


_update local package repo_
```bash
sudo apt update
```

_add additional packages_
```bash
sudo apt install -y bash-completion software-properties-common
```

---

###### Install Docker (if not already installed)

_Install Docker using a Rancher provided script_
```bash
curl https://releases.rancher.com/install-docker/20.10.sh | sudo sh
```

_add user to the docker group_
```bash
sudo usermod -aG docker $USER
```

__Note: You must logout and log back in to get the local shell to recognize the newly added 'docker' group__

---

###### Install Kubectl

_Install kubectl_
```bash
sudo snap install kubectl --classic
```

_Verify kubectl binary_
```bash
kubectl version
```

It can be helpful to enable tab-completion for 'kubectl'.  These commands will enable it.

```bash
source <(kubectl completion bash)
echo 'source <(kubectl completion bash)' >>~/.bashrc
```

---

###### Install RKE

_Install RKE binary_
```bash
sudo curl -L -o /usr/local/bin/rke https://github.com/rancher/rke/releases/download/v1.3.10/rke_linux-amd64
sudo chmod +x /usr/local/bin/rke
```

_Verify RKE binary_
```bash
rke --version
```

---

_Work in the ~/rke directory_
```bash
mkdir -p ~/rke
cd ~/rke
```

_configure RKE Cluster_
```bash
rke config --name cluster.yml
```

* SSH Address of host (1) = <publicIP>
* SSH Private Key Path of host (<publicIP>) = ~/.ssh/id_rsa
* Is host (<priveteIP>) a Control Plane host (y/n)? [y]: = y
* Is host (<privateIP>) a Worker host (y/n)? [n]: = y
* Is host (<privateIP>) an etcd host (y/n)? [n]: = y
* Internal IP of host (<privateIP>) [none]: = <privateIP>

###### Post config

* If using OpenEBS; change extra_binds to:

```yaml
services:
  kubelet:
    extra_binds:
      - /var/openebs/local:/var/openebs/local
```

---

##### Install the RKE Cluster

_Run the RKE up_
```bash
rke up
```

_Verfiy K8s cluster_
```bash
kubectl --kubeconfig ~/rke/kube_config_cluster.yml get nodes
```

_Configure kubectl (Re-run this on each new shell)_
```bash
export KUBECONFIG=~/rke/kube_config_cluster.yml
```

Or set the kubectl config in the standard location:
```bash
mkdir -p $HOME/.kube
cp ~/rke/kube_config_cluster.yml $HOME/.kube/config
```

_Verify cluster with kubectl_
```bash
kubectl get nodes
```
