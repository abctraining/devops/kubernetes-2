# Kubernetes Cluster with MicroK8s


```bash
sudo snap install microk8s --classic
```

```bash
sudo microk8s.status --wait-ready
```

```bash
sudo microk8s.enable dns
sudo microk8s.enable storage ingress metallb
```

_Use IP address range:_ '10.0.2.1-10.0.2.10'

```bash
sudo usermod -a -G microk8s $USER
```

__Note: You must logout and log back in to get the local shell to recognize the newly added 'microk8s' group__

Let's verify the versions of MicroK8s
```bash
microk8s.kubectl version
```

We also can install 'kubectl' directly instead of using 'microk8s.kubectl'.

```bash
sudo snap install kubectl --classic
```

Let's add a configuration for 'kubectl' to use

```bash
mkdir -p $HOME/.kube
microk8s config > $HOME/.kube/config
```

And verify it works

```
kubectl version
kubectl get nodes
```

It can be helpful to enable tab-completion for 'kubectl'.  These commands will enable it.

```bash
sudo apt install -y bash-completion
source <(kubectl completion bash)
echo 'source <(kubectl completion bash)' >>~/.bashrc
```
