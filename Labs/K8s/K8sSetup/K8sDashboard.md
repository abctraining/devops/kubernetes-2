## Kubernetes UI dashboard setup

Assumption:- Kubernetes is installed and configured

## Install dashboard

### Create resources required for dashboard


_dashboard_
```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.3.1/aio/deploy/recommended.yaml
```

<!-- _dashboard_
```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml
```

_create heapster: which is the only supported metrics provider currently_
```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/influxdb/heapster.yaml
```

_Influxdb backend for data collection_
```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/influxdb/influxdb.yaml
```

_create the heapster cluster role binding for the dashboard_
```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/rbac/heapster-rbac.yaml
``` -->

Create a yaml file named `dash-admin-service-account.yaml` to add an admin role to access dashboard.

```bash
vim ~/dash-admin-service-account.yaml
```

_dash-admin-service-account.yaml_
```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: dash-admin
  namespace: kubernetes-dashboard
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: dash-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: dash-admin
  namespace: kubernetes-dashboard
```

_create the resource_
```bash
kubectl apply -f ~/dash-admin-service-account.yaml
```

---

Now Kubernetes dashboard is deployed, and we have an administrator service account that can be used to view and control the cluster, now connect to the dashboard with that service account.

_Edit dashboard service to expose the service to view from outside_
```bash
kubectl -n kubernetes-dashboard edit service kubernetes-dashboard
```

Change type of service from 'ClusterIP' to 'NodePort' and save.


_Retrieve an authentication token for the dash-admin service account_
```bash
kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep dash-admin | awk '{print $1}')
```

Copy the token from above command.

_URL to access dashboard:_

`https://<public-ip>:<nodeport>/`

Where:

_public-ip_: IP of your machine

_nodeport_: port (for service "kubernetes-dashboard") can be accessed by running the command `kubectl get services -o wide -n kubernetes-dashboard`

There will be a self-signed SSL certificate that you will have to use.  Use the token from above to login to the dashboard.

---

## Test Dashboard

- Check 'cluster' - it shows namespaces, nodes, volumes, roles, storage classes, plus other "cluster info"

- Click on individual resources
- Check the 'overview', 'workloads' - for all namespaces (namespaces can be selected near the top of the page)
- Create nginx deployment using kubectl and check in dashboard
	```bash
	kubectl create deployment nginx --image=nginx --replicas=3
	```
	```bash
	kubectl expose deployment nginx --port=80 --type=NodePort
	```
- You will see 1 deployment, 3 pods, 1 replicaSet, 1 service
- Click on a resource & you will show details (same as `describe`)
- Scale the deployment to 5
- Confirm scaling in dashboard and CLI
- View/edit yaml for nginx deployment
- It opens an editor, change replica to 7
- Confirm scaling in dashboard and CLI
- Delete the nginx deployment and nginx service from dashboard
- Create a file and add below content, upload and confirm in dashboard and CLI
```bash
vim ~/nginx-deployment.yaml
```
_nginx-deployment.yaml_
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 2
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
        ports:
        - containerPort: 80
```
```bash
kubectl apply -f ~/nginx-deployment.yaml
```
- Expose as service in CLI:
```bash
kubectl expose deployment nginx-deployment --port=80 --type=NodePort
```
- Confirm in cli, dashboard and a browser
- Again delete the nginx deployment and service


*** What have we done.

In this exercise, we setup a dashboard, viewed and created resources using dashboard
