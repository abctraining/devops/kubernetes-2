# Simple Loadbalancer with MetalLB

### kubeadm clusters requirement

If your cluster is built with `kubeadm` there are some changes that need to be made or 'kube-proxy' to allow a floating IP to move between Kubernetes Nodes by allowing `strictARP`.

__This following step is only needed on kubeadm clusters__

_kubeadm: clusters, Set configure kube-proxy_
```bash
kubectl get configmap kube-proxy -n kube-system -o yaml | sed -e "s/strictARP: false/strictARP: true/" | kubectl create -f - -n kube-system
```

---

### Install MetalLB

We will apply two manifests; the first creates the `metallb-system` namespace, the second installs the all the MetalLB components.

_Install MetalLB_
```bash
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.9.5/manifests/namespace.yaml
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.9.5/manifests/metallb.yaml
```

The first time you install MetalLB you need to generate a "secretkey" that it uses internally.  __Only run this once.__  Changing the secret key later could break MetalLB communications between nodes.

_On first install Only_
```bash
kubectl create secret generic -n metallb-system memberlist --from-literal=secretkey="$(openssl rand -base64 128)"
```

MetalLB requires a config file to inform it about the network outside of Kubernetes.  Remember it is an "external" loadbalancer and thus needs to know how to connect to external networks.  In this example we are going to give it a very basic configuration using the Layer2 protocol and a private non-routable address pool.

_Configure MetalLB_
```bash
kubectl apply -f https://gist.githubusercontent.com/thejordanclark/17dbc5c26c5383bbfbba8df9c42339c6/raw/1731799c48e356615ddce50e3d2802000440a7ab/abc-k8s-metallb-config.yaml
```

If you are curious; here is the content of that MetalLB config that was just applied.

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: metallb-system
  name: config
data:
  config: |
    address-pools:
    - name: default
      protocol: layer2
      addresses:
      - 10.0.2.1-10.0.2.10
```

Now we can verify that all the MetalLB components have started up completely.

_Verify running pods_
```bash
watch kubectl get all,secrets,cm -n metallb-system
```

_Use 'CTRL+C' to exit the 'watch' command_
