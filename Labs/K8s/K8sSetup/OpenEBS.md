# Storage Controller with OpenEBS

#### Prepare Cluster

If your user does not have a cluster role binding create that binding first.  Mainly needed on RKE clusters.  Replace the following user with the correct user from your 'kubectl' config.

_Create the cluster binding_
```bash
kubectl create clusterrolebinding  cluster-admin-binding --clusterrole=cluster-admin --user=kube-admin-local
```

On a Single Node Cluster we need to remove the master taint.

_remove taint: Single Node Cluster_
```bash
 kubectl taint nodes --all node-role.kubernetes.io/master-
```

## Install OpenEBS

Use __One_ of the following options:

---
### Option 1: Lite Installation
_only Local PV (hostpath and device)_


_Install the lite operator_
```bash
kubectl apply -f https://openebs.github.io/charts/openebs-operator-lite.yaml
```

_Install the lite StorageClass_
```bash
kubectl apply -f https://openebs.github.io/charts/openebs-lite-sc.yaml
```

_wait for the resources to start_
```bash
watch kubectl get all -n openebs
```

_Show the StorageClasses_
```bash
kubectl get sc
```

_optionally set a default StorageClass_
```bash
kubectl patch storageclass openebs-hostpath -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
```

__Note: Make sure you only set on StorageClass as "Default"__

---
### Option 2: Full Installation using Helm
_make sure helm is installed first_

First we need to setup open-iscsi

_Install and start open-iscsi_
```bash
sudo apt-get install -y open-iscsi
sudo systemctl enable iscsid
sudo systemctl start iscsid
```

__Note: make sure is installed__

_Install OpenEBS using Helm_
```bash
kubectl create namespace openebs
helm repo add openebs https://openebs.github.io/charts
helm repo update
helm install openebs --namespace openebs openebs/openebs
```

_wait for the resources to start_
```bash
watch kubectl get all -n openebs
```

_Show the StorageClasses_
```bash
kubectl get sc
```

_optionally set a default StorageClass_
```bash
kubectl patch storageclass openebs-hostpath -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
```

__Note: Make sure you only set on StorageClass as "Default"__
