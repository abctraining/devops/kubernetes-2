# Join Cluster to Rancher

###### Cluster Prep (RKE Only)
_RKE: Cluster, add clusterrolebinding_
```bash
kubectl create clusterrolebinding cluster-admin-binding --clusterrole cluster-admin --user kube-admin-local
```

---

###### Setup the cluster in Rancher

1.  Log into the Rancher UI.  

1.	From the "Home" Dashboard, click "Import Existing" in the Cluster section.

1.	Choose "Generic", to import any K8s Cluster.

1.	Enter a Cluster Name including your name to easily identify it as your cluster.

1.	Click Create.

1.	Copy the kubectl command to your clipboard and run it on a node where kubeconfig is configured to point to the cluster you want to import. If you are unsure it is configured correctly, run kubectl get nodes to verify before running the command shown in Rancher.

1.	If you are using self signed certificates, you will receive the message certificate signed by unknown authority. To work around this validation, copy the command starting with curl displayed in Rancher to your clipboard. Then run the command on a node where kubeconfig is configured to point to the cluster you want to import.

1.	When you finish you will see your cluster listed as "Active"

1.  To access the cluster dashboard select the three horizontal lines on the top left of the page, then select your cluster.


#### Troubles:

_Restart cattle-system pod_
```bash
kubectl -n cattle-system delete pod `kubectl -n cattle-system get pods | grep ^cattle-cluster-agent | awk '{ print $1 }'`
```
