# Create-Persistent-Volume

We will use the 'yaml' files from the repos `Labs/K8s/PersistentVolumes/` directory.

## Create your PersistentVolumes and PersistentVolumeClaims

To deploy the PVC, run:

```bash
kubectl apply -f mysql-pvc.yaml
kubectl apply -f wordpress-pvc.yaml
```

Check to see if your claims are bound, it will show "pending" until the pods are created that use the PersistentVolume:

```bash
kubectl get pvc

```

#### Setup MySQL

Create a Kubernetes Secret to store the password for the database:

```bash
kubectl create secret generic mysql --from-literal=password=12345
```

The mysql.yaml manifest describes a Deployment with a single instance MySQL Pod which will have the MYSQL_ROOT_PASSWORD environment variable whose value is set from the secret created. The mysql container will use the PersistentVolumeClaim and mount the persistent disk at /var/lib/mysql inside the container.


Deploy manifest file `mysql.yaml`

```bash
kubectl create -f mysql.yaml
```

Check to see if the pod is running

```bash
kubectl get pod -l app=mysql
```

Deploy the manifest `mysql-service.yaml`

```bash
kubectl create -f mysql-service.yaml
```

Check to see if service was created

```bash
kubectl get service mysql
```

## Deploy Wordpress

Use the `wordpress.yaml` manifest
This manifest describes a Deployment with a single instance WordPress Pod. This container reads the WORDPRESS_DB_PASSWORD environment variable from the database password Secret you created earlier.

This manifest also configures the WordPress container to communicate MySQL with the host address mysql:3306. This value is set on the WORDPRESS_DB_HOST environment variable. We can refer to the database as mysql, because of Kubernetes DNS allows Pods to communicate a Service by its name.

Deploy the Manifest

```bash
kubectl create -f wordpress.yaml
```

Check to see if the pod is running

```bash
kubectl get pod -l app=wordpress
```

---

###### Expose the wordpress service

In the previous step, you have deployed a WordPress container which is not currently accessible from outside your cluster as it does not have an external IP address. To expose your WordPress application to traffic from the internet using a load balancer (subject to billing), you need a Service with type:LoadBalancer.

Use the `wordpress-service.yaml`

```bash
kubectl create -f wordpress-service.yaml
```

Check to see if the pod is running

```bash
kubectl get svc -l app=wordpress
```

_output_
```
NAME        TYPE           CLUSTER-IP    EXTERNAL-IP   PORT(S)        AGE
wordpress   LoadBalancer   10.43.81.97   10.0.2.2      80:31235/TCP   7s
```

In the output above, the EXTERNAL-IP column will show the LoadBalancer IP address created for WordPress. If that IP is publicly routable you should able to connect to it.

If it is not routable publicly use the "PublicIP" of your Lab Instance along with the "NodePort" for the service. (the example above is port:31235)

## Visit your Wordpress blog

Use the AWS Machine IP with the NodePort to check the hosted WordPress service. `http://<PublicIP>:<NodePort>`

## Cleaning up

Delete the wordpress service

```bash
kubectl delete service wordpress
kubectl delete service mysql
kubectl delete deployment.apps/mysql
kubectl delete deployment.apps/wordpress
```

Delete the PersistantVolumes

```bash
kubectl delete pvc wordpress-volume-pvc
kubectl delete pvc mysql-volume-pvc
```

check pv
```bash
kubectl get pv
```

You will see "status:released"

```bash
kubectl delete secret mysql
```
