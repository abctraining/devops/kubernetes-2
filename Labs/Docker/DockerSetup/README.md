# Docker Setup

_update local package repo_
```bash
sudo apt update
```

_add additional packages_
```bash
sudo apt install -y bash-completion
```

_Install Docker using a Rancher provided script_
```bash
curl https://releases.rancher.com/install-docker/20.10.sh | sudo sh
```

_add user to the docker group_
```bash
sudo usermod -aG docker $USER
```

__Note: You must logout and log back in to get the local shell to recognize the newly added 'docker' group__
