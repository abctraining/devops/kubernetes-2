# Docker Build Example

### First image build

##### Download an example Docker Image
```shell
cd ~/
git clone -b 3.14 https://github.com/p42/s6-alpine-docker.git
cd s6-alpine-docker
```

##### Check out the Dockerfile
```shell
cat Dockerfile
```


##### Build the Image with the tag 'local/s6-alpnie:3.14'
```shell
docker build -t local/s6-alpine:3.14 .
```

##### List the images, notice the new image
```shell
docker image ls
```

##### Run a container with the new image
```shell
docker run -it --rm local/s6-alpine:3.14 sh
```

When finished you can type `exit` to exit the container.


### Create an addition image based of the first

##### Create a new Docker File
```shell
cd ~/
mkdir MyNewImage
cd MyNewImage
echo "FROM local/s6-alpine:3.14" > Dockerfile
echo "RUN apk add --no-cache bash" >> Dockerfile
echo "CMD /bin/bash" >> Dockerfile
```

##### Checkout the new docker file
```shell
cat Dockerfile
```

##### Build the new image tagged 'local/alpine-bash'
```shell
docker build -t local/alpine-bash .
```

##### Notice the new image in the host's local image store
```shell
docker image ls
```

##### Run the new image this time using the newly installed "bash" shell
```shell
docker run -it --rm local/alpine-bash bash
```

As before type `exit` to exit the container when you are finished.


### Make your own modifications

Use either `vim` or `nano` to edit the file.  If unsure of which one to use you probably want `nano`.  In the Dockerfile make changes as you choose.  I.E. install addition packages by adding them to the list.  (look at the other Dockerfiles for examples)

##### Using Vim:
```shell
vim Dockerfile
```

or

##### Using nano:
```shell
nano Dockerfile
```

**hint:**
If you want to see what packages are available to install on Alpine linux start one of the containers based on one of the Alpine images and run these commands to find packages:

```shell
apk update
apk list
```

You can even search for packages with `apk search` I.E.

```shell
apk search nginx
```

This image does utilize the **s6 overlay** which is a series of scripts that are designed to ease Docker image creation.  For instance scripts located in the image in `/etc/cont-init.d/` will be automatically run as the container starts.  More info can be found at the [s6 overlay GitHub page](https://github.com/just-containers/s6-overlay).

